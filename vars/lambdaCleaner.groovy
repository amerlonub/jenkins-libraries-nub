#!/usr/bin/env groovy

// Examples: 
//  - Normal execution (BY DEFAULT): lambdaCleaner('pruebas-lambda-cleaners', '2')
//  - DryMode (This mode isn't delete any, only show info): lambdaCleaner('pruebas-lambda-cleaners', '2', true)

def call(functionName, maxVersions, boolean dryMode = 'false' ) {
  sh """#!/bin/sh
      echo "Drymode: ${dryMode}"
      tmpFile=lambdaData.tmp
      maxVersions=${maxVersions}
      versionsInfo=\$(aws lambda list-versions-by-function --function-name ${functionName})
      if [ "\$?" != "0" ]
      then
        echo "unexpected error while get the lambda versions"
        exit 1
      fi
      echo \$versionsInfo | jq -r '.Versions[] | "\\(.FunctionArn)"' | sort  | grep -v '\$LATEST' > \$tmpFile
      versionsTotal=\$(cat \$tmpFile | wc -l)
      versionsToDeleteNum=\$((\$versionsTotal-\$maxVersions))
      if [ \$versionsToDeleteNum -lt 0 ]
      then
        versionsToDeleteNum=0
      fi
      echo "--------"
      echo "${functionName} (Total: \$versionsTotal - Exceeded By: \$versionsToDeleteNum)" 
      echo "--------"
      if [ \$versionsTotal -gt \$maxVersions ]
      then
        versionsToDelete=\$(head -\$versionsToDeleteNum \$tmpFile)
          for versionToDelete in \$versionsToDelete
          do
            echo \$versionToDelete 🔥 DELETED
            if [ ${dryMode} == false ]
            then
              aws lambda delete-function --function-name \$versionToDelete
            fi
          done
        tail -\$maxVersions \$tmpFile
      else
          echo "Nothing to do here, yay! ✅"
      fi
  """
}